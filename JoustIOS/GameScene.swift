//
//  GameScene.swift
//  JoustIOS
//
//  Created by Sukhwinder Rana on 2019-06-16.
//  Copyright © 2019 Sukhwinder Rana. All rights reserved.
//


import SpriteKit
import GameplayKit

class GameScene: SKScene {
//player
    let mother = SKSpriteNode(imageNamed: "cooking")
//levels
    
    let level1 = SKSpriteNode(color: SKColor.clear, size: CGSize(width:4000 , height: 20))
    let level2 = SKSpriteNode(color: SKColor.clear, size: CGSize(width:4000 , height: 20))
    let level3 = SKSpriteNode(color: SKColor.clear, size: CGSize(width:4000 , height: 20))
    var button: SKSpriteNode! = nil
    let pillar = SKSpriteNode(imageNamed: "pillar")
    
    // GAME STAT SPRITES
    let livesLabel = SKLabelNode(text: "Lives: ")
    let scoreLabel = SKLabelNode(text: "Score: ")
   
    
    
    // MATH VARIABLES
    var checkJump = false
    
    // GAME STATISTIC VARIABLES
    var lives = 5
    var score = 0
    
    override func didMove(to view: SKView) {
      let audio = SKAudioNode(fileNamed: "a.wav")
        addChild(audio)
        
        self.button = SKSpriteNode(imageNamed: "up")
        // Put it in the center of the scene
        self.button.position = CGPoint(x:350, y:400);

        
        // Set the background color of the app
        self.backgroundColor = SKColor.black;
        self.anchorPoint.x = 0
        self.anchorPoint.y = 0
        
        // MARK: Create a background image:
        // --------------------------
        let bgNode = SKSpriteNode(imageNamed: "b")
        bgNode.position = CGPoint(x:self.size.width/2,y:self.size.height/2)
        bgNode.setScale(1.2)
        bgNode.zPosition = -3

        //set Player position ---> at first level in center
        mother.position = CGPoint(x:self.size.width/2,y:110)
        mother.setScale(2)
        
        
        
        // set four levels position
        level1.position = CGPoint(x:180,y:self.size.height * 0.25)
        level2.position = CGPoint(x:180,y:self.size.height/2)
        
        level3.position = CGPoint(x:180,y:self.size.height * 0.75)
        level3.zPosition = -1
        level2.zPosition = -1
        level1.zPosition = -1
        
        
        
        // MARK: Add a lives label
        // ------------------------
        self.livesLabel.text = "Lives: \(self.lives)"
        self.livesLabel.fontName = "Avenir-Bold"
        self.livesLabel.fontColor = UIColor.yellow
        self.livesLabel.fontSize = 80;
        self.livesLabel.position = CGPoint(x:360,y:self.size.height-100)
        
        
        // MARK: Add a score label
        // ------------------------
        self.scoreLabel.text = "Score: \(self.score)"
        self.scoreLabel.fontName = "Avenir-Bold"
        self.scoreLabel.fontColor = UIColor.cyan
        self.scoreLabel.fontSize = 80;
        self.scoreLabel.position = CGPoint(x:1100,y:self.size.height - 100)
        
        
        // MARK: Add your sprites to the screen
        addChild(mother)
        addChild(self.livesLabel)
        addChild(self.scoreLabel)
        addChild(bgNode)
        addChild(level1)
        addChild(level2)
        addChild(level3)
        addChild(button)
        createGrass()
    }
 
    var timeOfLastUpdate:TimeInterval?
    
    override func update(_ currentTime: TimeInterval) {
        PlayerPositionUp()
        
        if(playergoingRight == true){
            self.mother.position.x = self.mother.position.x + 7
            self.mother.xScale = 1
        } else if(playergoingRight == false){
             self.mother.position.x = self.mother.position.x - 7
            self.mother.xScale = -1
        }
        
        if( self.mother.position.x > 1390){
             self.mother.position.x = 210
        } else  if( self.mother.position.x < 210){
            self.mother.position.x = 1390
        }
        if (timeOfLastUpdate == nil) {
            timeOfLastUpdate = currentTime
        }
        
        // print a message every 3 seconds
        var timePassed = (currentTime - timeOfLastUpdate!)
//         if (timePassed >= 2.0 && self.jumpPress == true) {
//
//            // timeOfLastUpdate = currentTime
//        }
        if (timePassed >= 2.0) {
            if(jumpPress  == true){
                 self.jumpPress = false
            }
            print("HERE IS A MESSAGE!")
            timeOfLastUpdate = currentTime
            // make food
            if(self.foods.count < 2){
            self.makeFood()
          //  self.audio1.run(SKAction.stop())
              //  self.audio1.run()
            }
            
            
        }
        
        
        
        // MARK: R2: detect collisions between cake and player
        
        for (arrayIndex, shop) in foods.enumerated() {
            shop.position.x = shop.position.x + 5
            
            if(shop.position.x > 1390){
                shop.position.x = 210
            }
            if (self.mother.intersects(shop) == true) {

               if(self.playerGoingUp == "down" || self.checkJump == true){
                     print("PLAYER COLLIDE WITH FOOD! from Top...")
                     makeCoin(pos: arrayIndex)
                     checkJump = false
               }
               else if (self.playerGoingUp == "no" && self.checkJump == false){
                    
                        self.lives = self.lives - 1
                        // 2. update the lives label
                        self.livesLabel.text = "Lives: \(self.lives)"
                        mother.position = CGPoint(x:self.size.width/2,y:100)
                if (self.lives == 1){
                    let audio1 = SKAudioNode(fileNamed: "loss.wav")
                    addChild(audio1)
                }

                        if (self.lives == 0) {
                            // DISPLAY THE YOU LOSE SCENE
                            let loseScene = GameOverScene(size: self.size)

                            // CONFIGURE THE LOSE SCENE
                            loseScene.scaleMode = self.scaleMode

                            // MAKE AN ANIMATION SWAPPING TO THE LOSE SCENE
                            let transitionEffect = SKTransition.flipHorizontal(withDuration: 2)
                            self.view?.presentScene(loseScene, transition: transitionEffect)

                        }
                
                
                }
                print("Removing food at position: \(arrayIndex)")
                
                self.foods.remove(at: arrayIndex)
                shop.removeFromParent()
                
         
               
               
            }
        }
        for (index, coin) in coins.enumerated(){
            
            if (self.mother.intersects(coin) == true) {
                print("PLAYER COLLISION WITH COIN!")
                
                // 1. increase the score
                self.score = self.score + 1
                
                self.scoreLabel.text = "Score: \(self.score)"
               
                if (self.score == 4){
                    let audio2 = SKAudioNode(fileNamed: "win.wav")
                    
                    addChild(audio2)
                }
             
                
    
                self.coins.remove(at: index)
                coin.removeFromParent()
                
                  if (self.score == 5) {
                // YOU WIN!!!!
                                    let winScene = WinScene(size: self.size)
                
                                    // CONFIGURE THE LOSE SCENE
                                    winScene.scaleMode = self.scaleMode
                
                                    // MAKE AN ANIMATION SWAPPING TO THE LOSE SCENE
                                    let transitionEffect = SKTransition.flipHorizontal(withDuration: 2)
                                    self.view?.presentScene(winScene, transition: transitionEffect)
                
                
                                    break;
            }
            
            
            }
        
     
    }
    }
    
    

    var StartingTouchPosY:CGFloat = 0.0
    var EndingTouchPosY:CGFloat = 0.0
    var playerLevel = 1
    var playerGoingUp = "no"
    var playergoingRight = true
    var jumpPress = false
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let locationTouched = touches.first
        
        if (locationTouched == nil) {
            return
        }
        
        let mousePosition = locationTouched!.location(in:self)
        let sprite  = self.atPoint(mousePosition)
        if(button.contains(mousePosition)){
            print("jump presses")
            self.jumpPress = true
            self.checkJump = true
            print(jumpPress)
          //  audio1.run(SKAction.stop())
           // audio2.run(SKAction.stop())
        }
        StartingTouchPosY = mousePosition.y
         print("MOUSE STARTING Y POSITION?  \(StartingTouchPosY)")
        
       
        
    }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
   
        let locationTouched = touches.first

        if (locationTouched == nil) {
            return
        }

        let mousePosition = locationTouched!.location(in:self)
        EndingTouchPosY = mousePosition.y

        print("MOUSE ENDING Y POSITION?  \(EndingTouchPosY)")
        var difference = EndingTouchPosY - StartingTouchPosY
         if( difference < 63 && difference >= 0){
            if(mousePosition.x < self.size.width/2){
                self.playergoingRight = false
            }
            if(mousePosition.x > self.size.width/2){
                self.playergoingRight = true
            }
        }
        
        if( difference > 63 ){
            print("drag done for upside")
            print("difference is: \(difference)")
            playerGoingUp = "up"
        }
        else  if( difference < 0 ){
            print("drag done for downside")
            print("difference is: \(difference)")
            playerGoingUp = "down"
        } else{
            print("drag is not done")
            playerGoingUp = "no"
        }
       

    }
    
    //MARK: funtion  to create random cakes
    var foods:[SKSpriteNode] = []
    
    func makeFood() {
        // lets add some cakes
        let shop = SKSpriteNode(imageNamed: "cake")
        shop.zPosition = 1
        shop.setScale(0.9)
        var randX = Int(CGFloat(arc4random_uniform(UInt32(self.size.width - 250))))
        if(foods.count != 0){
            for (i, b) in foods.enumerated(){
                var pos = Int(b.position.x)
                var diff = pos - randX
                if(diff <= 0){
                    diff = diff * (-1)
                }
                
                if(diff <= 163){
                    while(randX < 330 || randX > Int(size.width - 250)){
                        randX = Int(CGFloat(arc4random_uniform(UInt32(self.size.width - 270))))
                    }
                    
                }
            }
            
        }
       
        var randY = 0
        // Int(CGFloat(arc4random_uniform(UInt32(self.size.height - 400))))
        var appearLevel = Int.random(in: 1...4)
        while(appearLevel == playerLevel){
            appearLevel = Int.random(in: 1...4)
        }
        
        if (appearLevel == 1) {
            randY = 100
        }
        if (appearLevel == 2) {
            randY = Int(self.size.height * 0.28)
        }
        if (appearLevel == 3) {
            randY = Int(self.size.height * 0.53)
        }
        if (appearLevel == 4) {
            randY = Int(self.size.height * 0.78)
        }
        
        shop.position = CGPoint(x:randX, y:randY)
        
        // add the cat to the scene
        addChild(shop)
        
        // add the cat to the cats array
        self.foods.append(shop)

    }
    
    //make coin afterr touching food
    var coins:[SKSpriteNode] = []
    // var c = 0
    
    func makeCoin(pos:Int) {
        // lets add some cakes
        let coin = SKSpriteNode(imageNamed: "coin")
        coin.zPosition = 1
        var randX = Int(CGFloat(arc4random_uniform(UInt32(self.size.width - 250))))
        while(randX < 330 || randX > Int(size.width - 250)){
            randX = Int(CGFloat(arc4random_uniform(UInt32(self.size.width - 250))))
        }
        var randY = 0
        // Int(CGFloat(arc4random_uniform(UInt32(self.size.height - 400))))
        var appearLevel = Int.random(in: 1...4)
        if (appearLevel == 1) {
            randY = 100
        }
        if (appearLevel == 2) {
            randY = Int(self.size.height * 0.29)
        }
        if (appearLevel == 3) {
            randY = Int(self.size.height * 0.54)
        }
        if (appearLevel == 4) {
                randY = Int(self.size.height * 0.79)
        }
        
        coin.position = CGPoint(x:randX, y:randY)
        addChild(coin)
        self.coins.append(coin)
    }
    
    
    //create grass
    
    var x = 0
    func createGrass(){
        let coin1 = SKSpriteNode(imageNamed: "grass")
        var n = Int(1568/coin1.size.width)
        for a in 1...4{
            for k in 0...n{
                let coin = SKSpriteNode(imageNamed: "grass")
                coin.zPosition = 1
                var randX = x + Int(coin.size.width) - 4
                x = randX
                var randY = 0
                var lvl = a
                
                
                if (lvl == 1) {
                    randY = 20
                }
                if (lvl == 2) {
                    randY = Int(self.size.height * 0.245)
                }
                if (lvl == 3) {
                    randY = Int(self.size.height * 0.495)
                }
                if (lvl == 4) {
                    randY = Int(self.size.height * 0.745)
                }
                
                coin.position = CGPoint(x:randX, y:randY)
                addChild(coin)
            }
            x = 0
        }
    }
    
    
    
    
    
    //MARK: funtion  to Set player position after slide
    
    func PlayerPositionUp(){
        var value = self.playerGoingUp
        if(value == "up" ){
            self.playerLevel = self.playerLevel + 1
            playerGoingUp = "no"
        } else if( value == "down"){
            self.playerLevel = self.playerLevel - 1
            playerGoingUp = "no"
        }else if( value == "no"){
            //self.playerLevel = self.playerLevel
        }
        if(self.playerLevel == 1){
            if(jumpPress == false){
               self.mother.position.y = 100
            }else{
               self.mother.position.y = 400
            }
        }
        else if(self.playerLevel == 2){
            if(jumpPress == false){
               self.mother.position.y =  self.size.height * 0.29
            }else{
               self.mother.position.y =  self.size.height * 0.29 + 300
            }
        }
        else if(self.playerLevel == 3){
            if(jumpPress == false){
                 self.mother.position.y = self.size.height * 0.54
            }else{
                 self.mother.position.y = self.size.height * 0.54 + 300
            }
            
           
            
        }
        else if(self.playerLevel == 4){
            if(jumpPress == false){
                 self.mother.position.y = self.size.height * 0.79
            }else{
                 self.mother.position.y = self.size.height * 0.79 + 300
            }
           
            
        }
        
        if(self.playerLevel > 4){
            self.mother.position.y = 100
            self.playerLevel = 1
            
        }
        if(self.playerLevel < 0){
            self.mother.position.y = self.size.height * 0.79
            self.playerLevel = 4
            
        }
        
    }
    
}
